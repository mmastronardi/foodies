
# Foodies

a next.js app from the course of Maximilian Schwarzmüller in [Udemy](https://www.udemy.com/course/nextjs-react-the-complete-guide)


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/mmastronardi/foodies.git
```

Go to the project directory

```bash
  cd foodies
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run dev
```

