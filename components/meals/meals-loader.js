import styles from "./styles/meals-loader.module.css";

export default function MealsLoader() {
  return <p className={styles.loading}>Fetching Meals...</p>;
}
